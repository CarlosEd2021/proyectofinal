package DAO;

import model.Categoria;
import java.util.List;


public interface CategoriaDAO {
    /*Definir los métodos, como la clase en interface los métodos no se 
    implementan aquí, los métodos son de tipo  abstracto*/
    public List<Categoria> Listar();
    public Categoria editarCategoria(int id_cat_edit);
    public boolean guardarCategoria(Categoria categoria);
    public boolean borrarCategoria(int id_cat_borrar);
}
