<%@page import="java.sql.*"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la sesión en el controlador -->


<html>    <head>

        <style>
            *{
                padding: 0;
                margin: 0;

            }
            img{
                width: 50%;
            }
            body {
                background: url(assets/img/OIP.jpg);
                background-size: cover;
                background-repeat: no-repeat;
                margin: 0;
                height: 100vh;
            }
            .table{
                color: white;
                background: window;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </head>
    <body >      
        <%@include file="WEB-INF/vista-categoria/encabezado.jspf" %>   
        <%
            //CONECTANOD A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost:3306/bd_inventario";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String clave = "";
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, clave);
            PreparedStatement ps;
            //Emnpezamos Listando los Datos de la Tabla Usuario
            Statement smt;
            ResultSet rs;
            smt = con.createStatement();
            rs = smt.executeQuery("select * from tb_producto");
            //Creamo la Tabla:     
        %>

        <br>
        <div class="container">            
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">New Add</button>
        </div>  
        <div class="container" >        

            <table class="table table-responsive"  id="tablaDatos" style="margin-top: 30px">
                <thead>
                    <tr style="background: #a6e1ec">
                        <th class="text-center">ID</th>
                        <th class="text-center">NOMBRE PRODUCTO</th>
                        <th class="text-center">STOCK</th>
                        <th class="text-center">PRECIO</th>
                        <th class="text-center">UNIDAD DE MEDIDA</th>
                        <th class="text-center">ESTADO PRODUCTO</th>
                        <th class="text-center">CATEGORIA</th>
                        
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody id="tbodys">
                    <%
                        while (rs.next()) {
                    %>
                    <tr style="background: yellowgreen">
                        <td class="text-center" style=" width:  115px"><%= rs.getInt("id_producto")%></td>
                        <td class="text-center" style=" width:  15px"><%= rs.getString("nom_producto")%></td>
                        <td class="text-center" style=" width:  115px"><%= rs.getString("stock")%></td>
                        <td class="text-center" style=" width:  115px"><%= rs.getString("precio")%></td>
                        <td class="text-center" style=" width:  115px"><%= rs.getString("unidad_de_medida")%></td>
                        <td class="text-center" style=" width:  115px"><%= rs.getString("estado_producto")%></td>
                        <td class="text-center" style=" width:  115px"><%= rs.getString("categoria")%></td>
                        
                        <td class="text-center" style=" width:  175px">


                            <a href="updateProd.jsp?id=<%= rs.getInt("id_producto")%>" class="btn btn-primary">Modificar</a>
                            <a href="DeleteProd.jsp?id=<%= rs.getInt("id_producto")%>" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <%}%>
            </table>
        </div>        
        <div class="container">          
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                    <div class="modal-content">
                        <div class="modal-header">                            
                            <h4 class="modal-title" id="myModalLabel">Agregar Registro</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" method="post">
                                   <h1>Agregar Registro</h1>
            <br>
                
                 NOMBRE PRODUCTO:
                <input type="text" name="txtnom" class="form-control">
                STOCK:
                <input type="text" name="txtstock" class="form-control">
                PRECIO:
                <input type="text" name="txtprecio" class="form-control">
                UNIDAD DE MEDIDA:
                <input type="text" name="txtud_medida" class="form-control">
                ESTADO PRODUCTO:
                <input type="text" name="txtest_product" class="form-control">
                <div class="form-group">
                            <label ><strong>CATEGORIA:</strong></label>
                            <select class="form-control" id="estado" name="txtcat">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                            </select>
                          </div>
                
                
                <hr>
                <input type="submit" value="Agregar Nuevo Registro" class="btn btn-primary "/>
                <input type="button" class="btn btn-warning"  value="Consultar registros" onclick="window.location.href='CrudProducto.jsp'"/>
           
                                </div>
                            </form>

                        </div>
                    </div>                    
                </div>
            </div>
<%
            String nom,stock,precio,ud_medida,est_produc,cat;
     
                nom = request.getParameter("txtnom");
                stock = request.getParameter("txtstock");
                precio = request.getParameter("txtprecio");
                ud_medida = request.getParameter("txtud_medida");
                est_produc = request.getParameter("txtest_product");
                cat = request.getParameter("txtcat");
                
               
                if (nom != null && stock != null && precio != null && ud_medida != null && est_produc != null && cat != null ) {
                    ps = con.prepareStatement("insert into tb_producto(nom_producto,stock,precio,unidad_de_medida,estado_producto,categoria) values('" + nom + "','" + stock + "','" + precio  + "','" + ud_medida  + "','" + est_produc  + "','" + cat  + "')");
                    ps.executeUpdate();     
           response.sendRedirect("CrudProducto.jsp");
           
       }
       
       
%>


        </div> 

        <%@include file="WEB-INF/vista-categoria/pie.jspf" %>
        <script src="js/jquery.js" type="text/javascript"></script>             
        <script src="js/bootstrap.min.js" type="text/javascript"></script>        
    </body>
</html>


